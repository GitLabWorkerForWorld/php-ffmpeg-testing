<?php

    include 'vendor/autoload.php';

    // $res = overlayImage($mp4, $img, 50, 50, __DIR__. '/output.mp4', 29, 2, 1, 1, 1);
    $mp4 = 'mov.mp4';
    $img = 'frame.jpg';
    // echo exec('which ffmpeg');
    $ffmpeg = FFMpeg\FFMpeg::create([
        'ffmpeg.binaries' => '/usr/local/bin/ffmpeg',
        'ffprobe.binaries' => '/usr/local/bin/ffprobe',
        'timeout'          => 3600, // The timeout for the underlying process
        'ffmpeg.threads'   => 12,   // The number of threads that FFMpeg should use
    ]);

    $res = exec('ffmpeg -y -i '. __DIR__. '/frame.jpg -i '. __DIR__. '/mov.mp4 -filter_complex "[0]scale=trunc(iw/2)*2:trunc(ih/2)*2[outer];[1]scale=400:300[inner];[outer][inner]overlay=10:10" '. __DIR__. '/output.mp4');
    // var_dump($res);
    // $video = $ffmpeg->open($img);
    // $video ->filters()->synchronize()->watermark($mp4, array(
    //         'position' => 'relative',
    //         'bottom' => 50,
    //         'right' => 50,
    //     ));
    // var_dump('video', $video);
    // $format = new FFMpeg\Format\Video\X264('libmp3lame', 'libx264');
    // $video->save($format, 'ok.mp4');
    // $newVideo = $ffmpeg->open('mov.mp4');
    // $newVideo
    //     ->concat(array( __DIR__. '/mov.mp4', __DIR__. '/ok.mp4' ))
    //     ->saveFromSameCodecs('result.mp4', TRUE);
    // $format
    //     -> setKiloBitrate(1000)
    //     -> setAudioChannels(2)
    //     -> setAudioKiloBitrate(256);
    // $video->save($format, 'ok.mp4');

    // $newVideo = $ffmpeg->open($mp4);
    // $format = new FFMpeg\Format\Video\X264();
    // $format->setAudioCodec("libmp3lame");
    // $newVideo->save($format, 'ok.mp3');

    // $format = new FFMpeg\Format\Video\X264();
    // $newVideo
    //     ->concat(array('ok.mp3', 'ok.mp4'))
    //     ->saveFromDifferentCodecs($format, 'finnal.mp4');

    // $image = $ffmpeg->open($img);
    // $image
    //     ->filters()
    //     ->watermark('ok.mp4', array(
    //         'position' => 'relative',
    //         'bottom' => 50,
    //         'right' => 50,
    //     ));
    // $format = new FFMpeg\Format\Video\X264('aac');
    // $image->save($format, 'final.mp4');
    // echo  __DIR__. '/'. $img;
    // echo  __DIR__. '/'. $mp4;


    // $res = overlayImage(
    //     __DIR__. '/'. $img,
    //     __DIR__. '/'. $mp4,
    //     0,
    //     0,
    //     __DIR__. '/ok.mp4',
    //     29,
    //     2,
    //     1,
    //     1,
    //     1
    // );
    // var_dump($res);